//Calling dependencies: fs for file reading, path for path manipulation, discord.js for API connection and classes, config.json for token call.
const fs = require('node:fs');
const path = require('node:path');
const { Client, Collection, Events, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');

//Creating process const for error handling outside of commands.
const process = require('node:process');

//New client (bot) instance
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

// Error handling outside commands

//Creating commands collection and filtering files to take only .js files.
client.commands = new Collection();
const commandsPath = path.join(__dirname, 'commands/general');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

const wakeCommandsPath = path.join(__dirname, 'commands/wakeserver');
const wakeCommandFiles = fs.readdirSync(wakeCommandsPath).filter(file => file.endsWith('.js'));

//For loop to call files.
for (const file of commandFiles) {
	const filePath = path.join(commandsPath, file);
	const command = require(filePath);
	client.commands.set(command.data.name, command);
}
for (const wakeFile of wakeCommandFiles) {
	const wakeFilePath = path.join(wakeCommandsPath, wakeFile);
	const command = require(wakeFilePath);
	client.commands.set(command.data.name, command);
}

//Event called once when the client is online.
client.once(Events.ClientReady, () => {
	console.log(`Ready! Connected as ${client.user.tag}`);
});

//Event that listens to the channel and execute an interaction if the message is a command.
client.on(Events.InteractionCreate, async interaction => {
	if (!interaction.isChatInputCommand()) return;

	const command = client.commands.get(interaction.commandName);

	if (!command) return;

	//Generic command handler.
	try {
		await command.execute(interaction);
	} catch (error) {
		//If interaction already replied, the error will be skipped and a warning message will be printed in the console.
		if (interaction.replied || interaction.deferred) {
			console.log('WARNING: Interaction already replied or deferred. Skipping error response.');
			return;
		} else {
			await interaction.reply({ content: 'There was an error while executing this command.', ephemeral: true });
			console.error(error);

		}
	}
});

//Client login function to Discord.
client.login(token);
