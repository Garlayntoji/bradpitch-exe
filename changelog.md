# Release 23.10.1
Although the commit history states there's a v1.0, I do not consider it the first bot's release. Please consider this release as the first one.

Setup and running instructions/suggestions are located in the [[readme]].

## Features
### Commands
- Wake command using etherwake in a shell script.
- Sleep command using SSH in a shell script.
- Status command using SSH in a shell script: returns the number of established connections on the remote server's port 21025.
- Restart Zerotier command using SSH in a shell script.
- Ping command: returns the bot's latency.
- Misc. commands
- Dynamic configuration using a `config.json` file.
    - Dynamic configuration for:
        - Bot's ID
        - Home server's ID
        - Bot's Token
        - Remote server's MAC address
        - Remote server's IP address
        - Interface used by the Bot's host to connect to internet

#### All the commands are used using the slash commands feature.
