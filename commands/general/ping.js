const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Replies with Pong!'),
	async execute(interaction) {
		await interaction.deferReply();
		const ping = interaction.client.ws.ping;
		const pingEmbed = new EmbedBuilder()
			.setColor(0x2b72e3)
			.setDescription(`Pong! :ping_pong: \nBot's ping is **${ping} ms.**`)
			.setTimestamp()
			.setFooter({text: "/ping"});


		return interaction.editReply({embeds: [pingEmbed]});
	},
};
