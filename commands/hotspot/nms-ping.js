const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('nms-ping')
		.setDescription('Replies with Pong!'),
	async execute(interaction) {
		const ping = interaction.client.ws.ping;
		return interaction.reply(`Pong! :ping_pong: \nBot's ping is -kzzt- ${ping} ms. -kzzt-`);
	},
};
