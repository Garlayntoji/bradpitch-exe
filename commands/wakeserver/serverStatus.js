const { spawn, exec } = require('child_process');
const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('server_status')
    .setDescription('Returns the remote PC\'s current status.'),
  async execute(interaction) {
    await interaction.deferReply();
    try {
      exec('sh serverStatus.sh', (error, stdout, stderr) => {
          const out = stdout.trim(); // Pour enlever les éventuels espaces indésirables
          console.log(out);
          if (error) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0xe10606)
                  .setDescription("The command returned an error. Please contact the bot's administrator.")
                  .setTimestamp()
                  .setFooter({text: "/server_status"});
              console.error(`Wake: Execution error : ${error.message}`);
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else if (stderr) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0xe10606)
                  .setDescription("The command returned an error. Please contact the bot's administrator.")
                  .setTimestamp()
                  .setFooter({text: "/server_status"});
              console.error(`Wake: Bash execution error: ${stderr}`);    
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else if (stdout.includes("online")) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0x2b72e3)
                  .setDescription("The server is :green_circle: **UP** :green_circle:.")
                  .setTimestamp()
                  .setFooter({text: "/server_status"});
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0x2b72e3)
                  .setDescription("The server is :red_circle: **DOWN** :red_circle:.")
                  .setTimestamp()
                  .setFooter({text: "/serverStatus"});
              return interaction.editReply({embeds: [statusEmbed]});
          }
      });        
    }
    catch (err) {
        console.error(err);
        statusEmbed.setColor(0xe10606);
        statusEmbed.setDescription("The command returned an error. Please contact the bot's administrator.")    
        return interaction.editReply({embeds: [statusEmbed]});
    }
  },
};
