const { spawn, exec } = require('child_process');
const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');
const { maxPlayers } = require('../../sbConfig.json');

const statusEmbed = new EmbedBuilder()
  .setTitle("Server status")
  .setAuthor({name: "Brad Pitch.exe", iconURL: "https://cdn.discordapp.com/avatars/500320359489208320/772e849d3fc26ea493285362b37e0c49?size=1024"})
  .setTimestamp()
  .setFooter({text: "/serverStatus"});

function getTimestamp() {
    const now = new Date();
    return now.toTimeString().split(' ')[0]; // Format HH:MM:SS
}

module.exports = {
  data: new SlashCommandBuilder()
    .setName('starbound_status')
    .setDescription('Returns the Starbound instance\'s current status.'),
  async execute(interaction) {
    await interaction.deferReply();
    try {
      exec('sh starboundStatus.sh', (error, stdout, stderr) => {
          const out = stdout.trim(); // Pour enlever les éventuels espaces indésirables
          console.log(out);
          if (error) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0xe10606)
                  .setDescription("The command returned an error. Please contact the bot's administrator.")
                  .setTimestamp()
                  .setFooter({text: "/starbound_status"});
              console.error(`[${getTimestamp()}] StarboundStatus: Execution error : ${error.message}`);
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else if (stderr) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0xe10606)
                  .setDescription("The command returned an error. Please contact the bot's administrator.")
                  .setTimestamp()
                  .setFooter({text: "/starbound_status"});
              console.error(`[${getTimestamp()}]StarboundStatus: Bash execution error: ${stderr}`);    
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else if (stdout.includes("offline")) {
            var statusEmbed = new EmbedBuilder()
                .setColor(0x2b72e3)
                .setDescription("The Starbound instance is :red_circle: **DOWN** :red_circle:.")
                .setTimestamp()
                .setFooter({text: "/starbound_status"});
            return interaction.editReply({embeds: [statusEmbed]});
        }
          else {
            var statusEmbed = new EmbedBuilder()
                .setColor(0x2b72e3)
                .setDescription(`The Starbound instance is :green_circle: **UP** :green_circle:.\nConnected players:**${stdout.trim().split(' ')[7]}/${maxPlayers}**.`)
                .setTimestamp()
                .setFooter({text: "/starbound_status"});
            return interaction.editReply({embeds: [statusEmbed]});
          }
          
      });        
    }
    catch (err) {
        console.error(err);
        statusEmbed.setColor(0xe10606);
        statusEmbed.setDescription("The command returned an error. Please contact the bot's administrator.")    
        return interaction.editReply({embeds: [statusEmbed]});
    }
  },
};
