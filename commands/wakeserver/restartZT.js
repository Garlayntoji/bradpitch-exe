const { exec } = require('child_process');
const { SlashCommandBuilder } = require('discord.js');
const { EmbedBuilder } = require('discord.js');

function getTimestamp() {
    const now = new Date();
    return now.toTimeString().split(' ')[0]; // Format HH:MM:SS
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('restart_zt')
        .setDescription('Restarts Zerotier on the server. Please use this when you cannot connect to the server.'),
        
    async execute(interaction) {
        await interaction.deferReply();
        
        try {
            exec('sh restartZT.sh', (error, stdout, stderr) => {
                const out = stdout.trim(); // Enlever les espaces indésirables
                console.log(out);

                if (error) {
                    var restartEmbed = new EmbedBuilder()
                        .setColor(0xe10606)
                        .setDescription("The command returned an error. Please contact the bot's administrator.")
                        .setTimestamp()
                        .setFooter({text: "/restart_zt"});
                    console.error(`[${getTimestamp()}] RestartZT: Execution error : ${error.message}`);
                    return interaction.editReply({embeds: [restartEmbed]});
                } else if (stderr) {
                    var restartEmbed = new EmbedBuilder()
                        .setColor(0xe10606)
                        .setDescription("The command returned an error. Please contact the bot's administrator.")
                        .setTimestamp()
                        .setFooter({text: "/restart_zt"});
                    console.error(`[${getTimestamp()}] RestartZT: Bash execution error: ${stderr}`);
                    return interaction.editReply({embeds: [restartEmbed]});
                } else if (stdout.includes("offline")) {
                    var restartEmbed = new EmbedBuilder()
                        .setColor(0xe10606)
                        .setDescription("The command was not executed. Please check if the server is already running.")
                        .setTimestamp()
                        .setFooter({text: "/restart_zt"});
                    return interaction.editReply({embeds: [restartEmbed]});
                } else { // Vérification si stdout contient une chaîne spécifique pour le succès
                    var restartEmbed = new EmbedBuilder()
                        .setColor(0x2b72e3)
                        .setDescription("Zerotier has been successfully restarted on the server.")
                        .setTimestamp()
                        .setFooter({text: "/restart_zt"});
                    return interaction.editReply({embeds: [restartEmbed]});
                }
            });
        } catch (err) {
            console.error(err);
            var restartEmbed = new EmbedBuilder()
                .setColor(0xe10606)
                .setDescription("An unexpected error occurred. Please contact the bot's administrator.")
                .setTimestamp()
                .setFooter({text: "/restart_zt"});
            return interaction.editReply({embeds: [restartEmbed]});
        }
    },
};
