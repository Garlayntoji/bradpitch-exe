const { spawn, exec } = require('child_process');
const { SlashCommandBuilder } = require('discord.js');
const { EmbedBuilder } = require('discord.js');

function getTimestamp() {
    const now = new Date();
    return now.toTimeString().split(' ')[0]; // Format HH:MM:SS
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('wake')
        .setDescription('Wakes the remote PC.'),
    async execute(interaction) {
        await interaction.deferReply();
        try {

            exec('sh wake.sh', (error, stdout, stderr) => {
                const out = stdout.trim(); // Pour enlever les éventuels espaces indésirables
                console.log(out);
                if (error) {
                    var wakeEmbed = new EmbedBuilder()
                        .setColor(0xe10606)
                        .setDescription("The command returned an error. Please contact the bot's administrator.")
                        .setTimestamp()
                        .setFooter({text: "/wake"});
                    console.error(`[${getTimestamp()}] Wake: Execution error : ${error.message}`);
                    return interaction.editReply({embeds: [wakeEmbed]});
                }
                else if (stderr) {
                    var wakeEmbed = new EmbedBuilder()
                        .setColor(0xe10606)
                        .setDescription("The command returned an error. Please contact the bot's administrator.")
                        .setTimestamp()
                        .setFooter({text: "/wake"});
                    console.error(`[${getTimestamp()}] Wake: Bash execution error: ${stderr}`);    
                    return interaction.editReply({embeds: [wakeEmbed]});
                }
                else if (stdout.includes("Not executed")) {
                    var wakeEmbed = new EmbedBuilder()
                        .setColor(0xe10606)
                        .setDescription("The command was not executed: the server is already online.")
                        .setTimestamp()
                        .setFooter({text: "/wake"});
                    return interaction.editReply({embeds: [wakeEmbed]});
                }
                else {
                    var wakeEmbed = new EmbedBuilder()
                        .setColor(0x2b72e3)
                        .setDescription("The server is now online.")
                        .setTimestamp()
                        .setFooter({text: "/wake"});
                    return interaction.editReply({embeds: [wakeEmbed]});
                }
                
            });        }
        catch (err) {
            console.error(err);
            wakeEmbed.setColor(0xe10606);
            wakeEmbed.setDescription("The command returned an error. Please contact the bot's administrator.")    
            return interaction.editReply({embeds: [wakeEmbed]});
        }
    },
};