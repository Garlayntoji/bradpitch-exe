const { spawn, exec } = require('child_process');
const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');

const { maxPlayers } = require('../../mcConfig.json');

function getTimestamp() {
  const now = new Date();
  return now.toTimeString().split(' ')[0]; // Format HH:MM:SS
}

module.exports = {
  data: new SlashCommandBuilder()
    .setName('minecraft_status')
    .setDescription('Returns the Minecraft instance\'s current status.'),
  async execute(interaction) {
    await interaction.deferReply();
    try {
      exec('sh minecraftStatus.sh', (error, stdout, stderr) => {
          const out = stdout.trim(); // Pour enlever les éventuels espaces indésirables
          console.log(out);
          if (error) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0xe10606)
                  .setDescription("The command returned an error. Please contact the bot's administrator.")
                  .setTimestamp()
                  .setFooter({text: "/minecraft_status"});
              console.error(`[${getTimestamp()}] MinecraftStatus: Execution error : ${error.message}`);
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else if (stderr) {
              var statusEmbed = new EmbedBuilder()
                  .setColor(0xe10606)
                  .setDescription("The command returned an error. Please contact the bot's administrator.")
                  .setTimestamp()
                  .setFooter({text: "/minecraft_status"});
              console.error(`[${getTimestamp()}] MinecraftStatus: Bash execution error: ${stderr}`);    
              return interaction.editReply({embeds: [statusEmbed]});
          }
          else if (stdout.includes("offline")) {
            var statusEmbed = new EmbedBuilder()
                .setColor(0x2b72e3)
                .setDescription("The Minecraft instance is :red_circle: **DOWN** :red_circle:.")
                .setTimestamp()
                .setFooter({text: "/minecraft_status"});
            return interaction.editReply({embeds: [statusEmbed]});
        }
          else {
            var statusEmbed = new EmbedBuilder()
                .setColor(0x2b72e3)
                .setDescription(`The Minecraft instance is :green_circle: **UP** :green_circle:.\nConnected players:**${stdout.trim().split(' ')[7]}/${maxPlayers}**.`)
                .setTimestamp()
                .setFooter({text: "/minecraft_status"});
            return interaction.editReply({embeds: [statusEmbed]});
          }
          
      });        
    }
    catch (err) {
        console.error(err);
        statusEmbed.setColor(0xe10606);
        statusEmbed.setDescription("The command returned an error. Please contact the bot's administrator.")    
        return interaction.editReply({embeds: [statusEmbed]});
    }
  },
};
