const { spawn, exec } = require('child_process');
const { SlashCommandBuilder } = require('discord.js');
const { execute } = require('../general/ping');
const { EmbedBuilder } = require('discord.js');

function getTimestamp() {
    const now = new Date();
    return now.toTimeString().split(' ')[0]; // Format HH:MM:SS
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('sleep')
        .setDescription('Suspends the remote PC.'),
        async execute(interaction) {
            await interaction.deferReply();
            try {
    
                exec('sh sleep.sh', (error, stdout, stderr) => {
                    const out = stdout.trim(); // Pour enlever les éventuels espaces indésirables
                    console.log(out);
                    if (error) {
                        var sleepEmbed = new EmbedBuilder()
                            .setColor(0xe10606)
                            .setDescription("The command returned an error. Please contact the bot's administrator.")
                            .setTimestamp()
                            .setFooter({text: "/sleep"});
                        console.error(`[${getTimestamp()}] Sleep: Execution error : ${error.message}`);
                        return interaction.editReply({embeds: [sleepEmbed]});
                    }
                    else if (stderr) {
                        var sleepEmbed = new EmbedBuilder()
                            .setColor(0xe10606)
                            .setDescription("The command returned an error. Please contact the bot's administrator.")
                            .setTimestamp()
                            .setFooter({text: "/sleep"});
                        console.error(`[${getTimestamp()}] Sleep: Bash execution error: ${stderr}`);    
                        return interaction.editReply({embeds: [sleepEmbed]});
                    }
                    else if (stdout.includes("offline")) {
                        var sleepEmbed = new EmbedBuilder()
                            .setColor(0xe10606)
                            .setDescription("The command was not executed: the server is already offline.")
                            .setTimestamp()
                            .setFooter({text: "/sleep"});
                        return interaction.editReply({embeds: [sleepEmbed]});
                    }
                    else {
                        var sleepEmbed = new EmbedBuilder()
                            .setColor(0x2b72e3)
                            .setDescription("The server is now offline.")
                            .setTimestamp()
                            .setFooter({text: "/sleep"});
                        return interaction.editReply({embeds: [sleepEmbed]});
                    }
                    
                });        }
            catch (err) {
                console.error(err);
                sleepEmbed.setColor(0xe10606);
                sleepEmbed.setDescription("The command returned an error. Please contact the bot's administrator.")    
                return interaction.editReply({embeds: [sleepEmbed]});
            }
        },
};