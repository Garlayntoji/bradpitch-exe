# Bradpitch.exe - A Discord Bot that manages waking and putting to sleep a single server
## Description - Features list
Bradpitch.exe is a discord bot that is meant to wake and suspend (aka put to sleep) a server. The purpose of such a program is to consume less power when the server isn't used.

The features that come with this bot are the following ones:
* Suspending a server (/sleep)
* Waking a server up (/wake)
* Check server status (up/down) and the number of active connections on a port specified in the `status.sh` file. => Particularly useful for checking the number of connected players in a game server without having to launch the game, or simply to check if anyone is using the server.
* Restart ZeroTier (/restart_zt). This is useful if your server is only accessible through ZeroTier, a VPN. Sometimes the zerotier-one service needs to be restarted on the server. You can delete this command if you don't use ZeroTier.
* Ping (/ping) : pings the bot with current latency. Useful for checking bot's status, if it crashed or not.
* Other small commands that will not be documented here.

## Installation Guide
### Required dependencies
You NEED Node.js v16.9.x and not any other version to launch the bot, otherwise you will be met with a crash. You will also need to run the bot on a Linux installation. The installation guide is based on a Raspberry Pi 3 B deployment.

Reason: the bot runs shell scripts that are understood by Linux and not Windows nor MacOS. Every shell script is available at the repo's root. Inside some of the scripts there are SSH commands that send Linux commands to the remote server. Naturally, this cannot be interpreted by Windows and MacOS installations.

### Prefacing notes
- The target server MUST run a Linux installation, preferably a Debian-based distribution (Debian, Ubuntu...)
- The target server and the bot's hosting machine (i.e the Raspberry Pi) MUST be on the same local network.
- The remote server MUST allow root login over SSH, using encryption keys. I know how not secure this method is, I'm currently searching for more secure ways.
- It is recommended to put the server on a fixed IP address, especially if you're going to port forward. The bot uses the server's IP address to suspend it and get its status so you might want to do that anyways. Note: you can let the server use DHCP to get its IP address in your network, be aware of potential IP changes once the DHCP lease time is expired.

### Server-side SSH setup

#### Disabling root login on the remote server
This part is optional if you haven't enabled the root login before, but it is a security precaution to explicitely disable root login via SSH.

On your remote server, edit the SSH config file (superuser privileges needed):
```bash
nano /etc/ssh/sshd_config
```
Uncomment this line and add "no" if it's not present:
```bash
PermitRootLogin no
```

#### Creating a new user able to execute specific power user commands
On your remote server, create a new user with the following command:
```bash
useradd <username>
```

In my case I'll use the username "remotepi". Then you'll want to give it authorization to execute some power user commands without needing a password. This is needed to avoid authentication each time you'll want to restart a specific service or suspend the remote server using the bot. Type in:

```bash
sudo visudo
```

And in the "# User privilege specification", paste the following lines:

```bash
remotepi ALL=(ALL) NOPASSWD: /bin/systemctl suspend
# Paste the next line only if using Zerotier One as the way of accessing your remote server by other people.
remotepi ALL=(ALL) NOPASSWD: /bin/systemctl restart zerotier-one.service
remotepi ALL=(ALL) NOPASSWD: /bin/netstat -na
```

Be sure to replace "remotepi" by your newly created username.

Reboot the remote server (not sure if it's needed but do it anyway just in case). Test the permissions while logged in your newly created user and make sure they don't require a password when executed with sudo.

### Quick installation
Clone the repo and go in it. Run the setup script by using the following commands:
```bash
chmod +x setup.sh
./setup.sh
```
Follow the prompted instructions from the script.

### Manual installation
This guide leads to the exact same configuration as the quick installation.

#### Generating the encryption keys
Generate a SSH key. Type in:
```bash
ssh-keygen -t rsa
```
Press enter on all fields, even the passphrase. The goal is to be able to login to the remote server without manual authentification.

You now need to send the public key of the key pair you generated to the remote server. Type in:
```bash
ssh-copy-id remotepi@<remote_server_ip_address>
```

Again, make sure to replace "remotepi" by the newly created username.

You should be able to log in via SSH without needing to enter a password now.

### Installing Node.js on the bot's hosting machine
To install the version of Node.js you need, you first have to install the Node Version Manager (NVM). To do this, enter the following command:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```
The command clones the nvm repository to \~/.nvm and adds the source line to your bash profile (\~/.bash_profile, \~/.zshrc, \~/.profile, or \~/.bashrc).

Run the following lines:
```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```
Now check if NVM is installed by typing `command -v nvm`. The command should return "nvm".

Install the required node version, then make sure to use it by typing:
```bash
nvm install 16.9
nvm use 16.9
```

### Installing the required node modules
First you need NPM, the node package manager. Install it with your distribution's package manager. For a Raspberry Pi running Raspberry Pi OS, it's `apt install npm`. (Superuser privileges are required to run this command.)

Once NPM is installed, go into the cloned repository and type in:
```bash
npm install
```

This will install all the packages specified in the `package.json` file. 

### Updating the scripts to target your server
First, get your remote server's MAC address and IP address. This can be checked using `ip a`, getting you an output like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 84:a9:38:fe:b7:fd brd ff:ff:ff:ff:ff:ff
    altname enp3s0
    inet 192.168.1.21/24 brd 192.168.1.255 scope global dynamic noprefixroute eno1
       valid_lft 25696sec preferred_lft 25696sec
    inet6 2a01:e0a:426:6120:a1:bf0f:c631:fb5d/64 scope global temporary dynamic 
       valid_lft 86080sec preferred_lft 68433sec
```
You want to focus on the 2nd interface (or another one if you have multiple interfaces, i.e wi-fi), in my example it's "eno1": the first one, "lo", is the loopback interface, in other words, the localhost.
- The MAC address on this example is `84:a9:38:fe:b7:fd`.
- The IP address on this example is `192.168.1.21`.

### (Optional) Removing ZeroTier restart
To remove the ZeroTier restart command, you can delete the `restart_zt.js` file in `commands/wakeserver`. You may want to delete this if you're deploying your server over the internet via port-forwarding or another VPN service, other than Zerotier.

### Configure the bot
Create a `config.json` file. It has to be like this:
```json
 {
	"clientId": "your bot user ID",
	"guildId": "your bot's home Discord server ID",
	"token": "your bot's token",
   "mac_address": "remote server's MAC address (must be in the same network)",
   "ip_address": "remote server's private IPv4 address (must be in the same network)",
   "interface": "interface the bot's host machine is connected to the network (i.e eth0, enp0s3, eno1...)"
}
```
Replace the fields with the intended values. Keep the quotes.

Create a `mcConfig.json` **AND** a `sbConfig.json` file. It has to be like this:
```json
{
   "maxPlayers":"max players on instance",
   "serverPort":"port used for connection"
}
```
Replace the fields with the intended values. Keep the quotes.


### Deploying commands and launching the bot
If you removed or added a command in `commands/general` or `commands/wakeserver` you have to deploy the commands. This will update the list of commands available on Discord. Type in:
```bash
node deploy-commands.js
```
Alternatively, if the above command didn't work or if you want to deploy commands not only in your bot's home server but in any server the bot has been invited, type in:
```bash
node deploy-global.js
```

Finally, launch the bot by typing:
```bash
node index.js
```

## Troubleshooting
If you encounter an issue, please open an issue on the Gitlab repository.

## License
Brad Pitch.exe is distributed under the GNU GPLv3 license - [see full text here](https://choosealicense.com/licenses/gpl-3.0/).