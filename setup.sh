#!/bin/bash

# config.json generation function
generate_config() {
    echo "Initiating bot initial configuration..."

    read -p "Client ID (ID of the bot on Discord) : " clientId
    read -p "Guild ID (ID of the 'home' Discord server) : " guildId
    read -p "Token (Bot's token) : " token
    read -p "MAC Address (Remote host's MAC) : " mac_address
    read -p "IP Address (Remote host's IP address) : " ip_address
    read -p "Interface (Bot host machine's network interface name) : " interface
    read -p "Remote User (Remote host user for SSH access) : " remote_user

    # Générer le fichier config.json
    cat <<EOF > config.json
{
    "clientId": "$clientId",
    "guildId": "$guildId",
    "token": "$token",
    "mac_address": "$mac_address",
    "ip_address": "$ip_address",
    "interface": "$interface",
    "remote_user": "$remote_user"
}
EOF

    echo "Configuration file config.json has been generated."
}

# mcConfig.json generation function
generate_mcConfig() {
    # Asking Starbound server information
    read -p "Max number of players (default: 20): " maxPlayers
    maxPlayers=${maxPlayers:-20}

    read -p "Server port (default: 2225565565): " serverPort
    serverPort=${serverPort:-25565}

    # Generating sbConfig.json
    cat <<EOF > mcConfig.json
{
    "maxPlayers": "$maxPlayers",
    "serverPort": "$serverPort"
}
EOF
    echo "Configuration file mcConfig.json has been generated."
}

# sbConfig.json generation function
generate_sbConfig() {
    # Asking Starbound server information
    read -p "Max number of players (default: 8): " maxPlayers
    maxPlayers=${maxPlayers:-8}

    read -p "Server port (default: 21025): " serverPort
    serverPort=${serverPort:-21025}

    # Generating sbConfig.json
    cat <<EOF > sbConfig.json
{
    "maxPlayers": "$maxPlayers",
    "serverPort": "$serverPort"
}
EOF
    echo "Configuration file sbConfig.json has been generated."
}

# Verify if config.json already exists
if [ -f "config.json" ]; then
    read -p "The configuration file config.json already exists. Do you want to overwrite it? (y/n): " overwrite
    if [[ "$overwrite" == "y" || "$overwrite" == "Y" ]]; then
        generate_config
    else
        echo "Skipping config.json generation."
    fi
else
    generate_config
fi

ip_address=$(jq -r '.ip_address' config.json)
remote_user=$(jq -r '.remote_user' config.json)

# Asks if keeping Minecraft server commands

# Asks if keeping Minecraft server commands
read -p "Do you want to monitor a Minecraft server? (y/n):" minecraft
if [[ "$minecraft" == "n" || "$minecraft" == "N" ]]; then
    rm -f minecraftStatus.sh
    rm -f commands/wakeserver/minecraftStatus.js
    echo "Minecraft-related files deleted."
else
    if [ -f "mcConfig.json" ]; then
        read -p "The configuration file mcConfig.json already exists. Do you want to overwrite it? (y/n): " overwrite
        if [[ "$overwrite" == "y" || "$overwrite" == "Y" ]]; then
            generate_mcConfig
        else
            echo "Skipping mcConfig.json generation."
        fi
    else
        generate_mcConfig
    fi
fi

# Asks if keeping Starbound server commands
read -p "Do you want to monitor a Starbound server? (y/n):" starbound
if [[ "$starbound" == "n" || "$starbound" == "N" ]]; then
    rm -f starboundStatus.sh
    rm -f commands/wakeserver/starboundStatus.js
    echo "Starbound-related files deleted."
else
    if [ -f "sbConfig.json" ]; then
        read -p "The configuration file sbConfig.json already exists. Do you want to overwrite it? (y/n): " overwrite
        if [[ "$overwrite" == "y" || "$overwrite" == "Y" ]]; then
            generate_sbConfig
        else
            echo "Skipping sbConfig.json generation."
        fi
    else
        generate_sbConfig
    fi
fi


# Ask if keeping Zerotier commands
read -p "Is Zerotier used for connection over Internet? (y/n):" zerotier
if [[ "$zerotier" == "n" || "$zerotier" == "N" ]]; then
    rm -f restartZT.sh
    rm -f commands/wakeserver/restartZT.js
    echo "Zerotier-related files deleted."
fi

# Ask if generating SSH keys for remote host
read -p "Do you want to generate new SSH keys for the remote host connection? (y/n):" ssh_keygen
if [[ "$ssh_keygen" == "y" || "$ssh_keygen" == "Y" ]]; then
    echo "When asked for passphrase, press only Enter to add no passhphrase to the SSH key."
    ssh-keygen -t ed25519
fi
ssh-copy-id $remote_user@$ip_address

# Dependencies installation process
echo "Installing Node.js..."
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install 16.9
nvm use 16.9
npm install

# Uploading commands on Discord
echo "Updating slash {/} commands for the bot on Discord..."
node deploy-commands.js
read -p "Do you want to deploy the commands on all the Discord servers the bot has joined? (y/n)" deploy_global
if [[ "$deploy_global" == "y" || "$deploy_global" == "Y" ]]; then
    node deploy-global.js
fi

echo "Installation finished."