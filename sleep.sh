#!/bin/bash

ip_address=$(jq -r '.ip_address' config.json)
remote_user=$(jq -r '.remote_user' config.json)

if ping -c 1 -w 1 $ip_address >/dev/null; then
    out=$(ssh $remote_user@$ip_address "sudo systemctl suspend" 2>/dev/null)
    if [ $? -ne 0 ]; then
        echo "[$(date +"%H:%M:%S")] Sleep: Error when executing ssh: $out" >&2
        exit 1
    else
        echo "[$(date +"%H:%M:%S")] Sleep: Server successfully suspended."
        exit 0
    fi
else
    echo "[$(date +"%H:%M:%S")] Sleep: Not executed: server already offline"
    exit 0
fi
