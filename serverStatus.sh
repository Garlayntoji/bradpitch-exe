#!/bin/bash

ip_address=$(jq -r '.ip_address' config.json)
remote_user=$(jq -r '.remote_user' config.json)

if ping -c 1 -w 1 $ip_address >/dev/null; then
    echo "[$(date +"%H:%M:%S")] ServerStatus: online"
    #ssh $remote_user@$ip_address "sudo netstat -na | grep 21025 | grep "ESTABLISHED" | grep -E "tcp6" | wc -l" > playercount.txt
else
    echo "[$(date +"%H:%M:%S")] ServerStatus: offline"
fi
