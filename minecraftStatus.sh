#!/bin/bash

ip_address=$(jq -r '.ip_address' config.json)
remote_user=$(jq -r '.remote_user' config.json)

serverPort=$(jq -r '.serverPort' mcConfig.json)

if ping -c 1 -w 1 "$ip_address" >/dev/null && \
   [ $(( $(ssh "$remote_user@$ip_address" "netstat -na | grep ':"$serverPort" .*LISTEN' | wc -l" | tr -d '\n') )) -ge 1 ]; then
    players_connected=$(ssh $remote_user@$ip_address "sudo netstat -na | grep "$serverPort" | grep "ESTABLISHED" | grep -E "tcp6" | wc -l")
    if [ $? -ne 0 ]; then
        echo "[$(date +"%H:%M:%S")] MinecraftStatus: Error when executing ssh."
        exit 1
    else
        echo "[$(date +"%H:%M:%S")] MinecraftStatus: Command executed successfully. Players connected: $players_connected"
        exit 0
    fi
else
    echo "[$(date +"%H:%M:%S")] MinecraftStatus: offline"
fi
