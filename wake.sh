#!/bin/bash

mac_address=$(jq -r '.mac_address' config.json)
interface=$(jq -r '.interface' config.json)
ip_address=$(jq -r '.ip_address' config.json)


if ! ping -c 1 -w 1 $ip_address >/dev/null; then
    
    out=$(wakeonlan $mac_address 2>&1)
    if [ $? -ne 0 ]; then
        echo "[$(date +"%H:%M:%S")] Wake: Error when executing wakeonlan: $out" >&2
        exit 1
    else
        echo "[$(date +"%H:%M:%S")] $out"
        exit 0
    fi
else
    echo "[$(date +"%H:%M:%S")] Wake: Not executed: server already online."
    exit 0
fi
